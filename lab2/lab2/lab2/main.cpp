#include <conio.h>
#include <chrono>
#include <iostream>
#include <iomanip>
#include <math.h>
#include <string>
#include <cstring>
#include <Windows.h>
#include <cstdlib>

using namespace std;

const int MaxLen = 255;

string InitialText;
bool Ending;
int i, j, CountOfSentences, LengthsOfSen[MaxLen], StartOfLen[MaxLen], EndOfLen[MaxLen];

void EnterText()
{
	cout << "Write your text:" << endl;
	cin.get();
	getline(cin, InitialText);
	system("pause");
}

void GetSettings()
{
	int StartPosition, NumberOfSentences;
	NumberOfSentences = StartPosition = CountOfSentences = 0;
	for (i = 0; i <= InitialText.length(); i++)
	{
		if (InitialText[i] == '.')
		{
			CountOfSentences++;
		}
	}
	for (i = 0; i <= InitialText.length(); i++)
	{
		if (InitialText[i] == '.')
		{
			NumberOfSentences++;
			StartOfLen[NumberOfSentences] = StartPosition;
			EndOfLen[NumberOfSentences] = i;
			LengthsOfSen[NumberOfSentences] = i - StartPosition;
			StartPosition = i + 2;
		}
	}
}

void TransmitToTwelve()
{
	int x, l, g;
	string FirstSentence, s;
	cout << "Entered text: " << endl;
	cout << InitialText << endl;
	cout << endl;
	GetSettings();
	size_t digits = InitialText.find_first_of("1234567890+-");
	if (digits <= InitialText.size())
	{
		cout << "Number found: " << atoi(InitialText.c_str() + digits) << endl;
		x = atoi(InitialText.c_str() + digits);
		while (x > 0)
		{
			g = x % 12;
			switch (g)
			{
			case 10: { s += 'P' + '0'; break; };
			case 11: { s += 'R' + '0'; break; };
			default: { s += g + '0'; };
			}
			x /= 12;
		}
		l = s.length();
		cout << "Number in 12'th number system: ";
		for (int i = 0; s[i]; i++)
		{
			cout << s[l - i - 1];
		}
	}
	else
	{
		cout << "Number is not found" << endl;
	}
	cout << endl;
	system("pause");
}

void CountOfWords()
{
	string word, bufferword;
	int WordsCount, WordStart[MaxLen], WordEnd[MaxLen], WordLen[MaxLen], StartPosition, Count;
	cout << "Entered text: " << endl;
	cout << InitialText << endl;
	cout << endl;
	cout << "Enter word: " << endl;
	cin.get();
	getline(cin, word);
	StartPosition = WordsCount = Count = 0;
	for (i = 0; i <= InitialText.length(); i++)
	{
		if ((InitialText[i] == ' ') && (InitialText[i - 1] != '.'))
		{
			WordsCount++;
			WordStart[WordsCount] = StartPosition;
			WordEnd[WordsCount] = i - 1;
			WordLen[WordsCount] = i - StartPosition;
			StartPosition = i + 1;
		}
		if (InitialText[i] == '.')
		{
			WordsCount++;
			WordStart[WordsCount] = StartPosition;
			WordEnd[WordsCount] = i - 1;
			WordLen[WordsCount] = i - StartPosition;
			StartPosition = i + 2;
		}
	}
	for (i = 1; i <= WordsCount; i++)
	{
		for (j = WordStart[i]; j <= WordEnd[i]; j++)
		{
			bufferword += InitialText[j];
		}
		if (bufferword == word)
		{
			Count++;
		}
		bufferword = "";
	}
	cout << endl;
	if (Count != 0)
	{
		cout << "Count of matching words: " << Count << endl;
	}
	else
	{
		cout << "No matching words!" << endl;
	}
	system("pause");
}


void Menu()
{
	system("cls");
	int EnterKey;
	cout << "Entered text: " << endl;
	cout << InitialText << endl;
	cout << endl;
	cout << "Select menu item" << endl;
	cout << "1: Enter the text" << endl;
	cout << "4: Transmit 1st number to twelves" << endl;
	cout << "5: Count of matching words" << endl;
	cout << "7: Exit";
	cout << endl;
	cout << "->";
	cin >> EnterKey;
	system("cls");
	switch (EnterKey)
	{
	case 1: { EnterText(); break; };
	case 4: { TransmitToTwelve(); break; };
	case 5: { CountOfWords(); break; };
	case 7: { Ending = 0; break; };
	default:
	{
		cout << "ERROR: NON-EXISTENT MENU ITEM!" << endl;
		system("pause");
		system("cls");
	}
	}
}

void main()
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hConsole, 15);
	Ending = 1;
	while (Ending == 1)
	{
		Menu();
	}
}